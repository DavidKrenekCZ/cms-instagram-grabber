<?php

namespace Modules\Instagramgrabber\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Instagramgrabber\Entities\InstagramGrabber;
use Modules\Instagramgrabber\Entities\Settings;
use Modules\Instagramgrabber\Http\Requests\CreateInstagramGrabberRequest;
use Modules\Instagramgrabber\Http\Requests\UpdateInstagramGrabberRequest;
use Modules\Instagramgrabber\Repositories\InstagramGrabberRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class InstagramGrabberController extends AdminBaseController
{
    /**
     * @var InstagramGrabberRepository
     */
    private $instagramgrabber;

    public function __construct(InstagramGrabberRepository $instagramgrabber)
    {
        parent::__construct();

        $this->instagramgrabber = $instagramgrabber;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $settings = Settings::all();

        return view('instagramgrabber::admin.instagramgrabbers.index', compact('settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('instagramgrabber::admin.instagramgrabbers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateInstagramGrabberRequest $request
     * @return Response
     */
    public function store(CreateInstagramGrabberRequest $request)
    {

        $this->instagramgrabber->create($request->all());

        return redirect()->route('admin.instagramgrabber.instagramgrabber.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('instagramgrabber::instagramgrabbers.title.instagramgrabbers')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  InstagramGrabber $instagramgrabber
     * @return Response
     */
    public function edit(InstagramGrabber $instagramgrabber)
    {
        return view('instagramgrabber::admin.instagramgrabbers.edit', compact('instagramgrabber'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  InstagramGrabber $instagramgrabber
     * @param  UpdateInstagramGrabberRequest $request
     * @return Response
     */
    public function update(InstagramGrabber $instagramgrabber, UpdateInstagramGrabberRequest $request)
    {
        foreach ($request->id as $key => $settings_id){
            $settings_row = Settings::find($settings_id);
            $settings_row->hashtag = $request->hashtag[$key];
            $settings_row->save();
        }
        return redirect()->route('admin.instagramgrabber.instagramgrabber.index')
            ->withSuccess(trans('instagramgrabber::instagramgrabbers.messages.saved'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  InstagramGrabber $instagramgrabber
     * @return Response
     */
    public function destroy(InstagramGrabber $instagramgrabber)
    {
        $this->instagramgrabber->destroy($instagramgrabber);

        return redirect()->route('admin.instagramgrabber.instagramgrabber.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('instagramgrabber::instagramgrabbers.title.instagramgrabbers')]));
    }
}
