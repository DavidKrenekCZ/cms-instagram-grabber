<?php

return [
    'list resource' => 'Zobrazení a úpravy',
    /*'create resource' => 'Create instagramgrabbers',
    'edit resource' => 'Edit instagramgrabbers',
    'destroy resource' => 'Destroy instagramgrabbers',*/
    'title' => [
        'main' => 'Instagram fotky',
        'hashtag' => 'Hashtag',
        'create instagramgrabber' => 'Create a instagramgrabber',
        'edit instagramgrabber' => 'Edit a instagramgrabber',
    ],
    'button' => [
        'create instagramgrabber' => 'Create a instagramgrabber',
    ],
    'table' => [
        'block_name' => 'Název bloku',
        'hashtag' => 'Hashtag',
    ],
    'form' => [
    ],
    'messages' => [
        'saved' => 'Nastavení bylo úspěšně uloženo',
    ],
    'validation' => [
    ],
];
