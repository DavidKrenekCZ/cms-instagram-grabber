<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class FillInstagramGrabberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        for ($i = 1; $i <= 2; $i++) {
            DB::table('instagramgrabber__settings')->insert(
                [
                    'block_name'    => 'Blok '.$i,
                    'hashtag'      => 'Brno',
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        if (Schema::hasTable('instagramgrabber__settings'))
            DB::table('instagramgrabber__settings')->truncate();
    }
}
