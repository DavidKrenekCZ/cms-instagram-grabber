<?php

return [
    'list resource' => 'List & edit',
    /*'create resource' => 'Create instagramgrabbers',
    'edit resource' => 'Edit instagramgrabbers',
    'destroy resource' => 'Destroy instagramgrabbers',*/
    'title' => [
        'main' => 'Instagram photos',
        'hashtag' => 'Hashtag',
        'create instagramgrabber' => 'Create a instagramgrabber',
        'edit instagramgrabber' => 'Edit a instagramgrabber',
    ],
    'button' => [
        'create instagramgrabber' => 'Create a instagramgrabber',
    ],
    'table' => [
        'block_name' => 'Block name',
        'hashtag' => 'Hashtag',
    ],
    'form' => [
    ],
    'messages' => [
        'saved' => 'Setting successfully saved',
    ],
    'validation' => [
    ],
];
