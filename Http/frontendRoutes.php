<?php

use Illuminate\Routing\Router;

/** @var Router $router */
if (! App::runningInConsole()) {
    $router->get('/instagramgrabber/cron', [
        'as' => 'instagramgrabber.cron',
        'middleware' => config('asgard.page.config.middleware'),
        'uses' => 'CronController@updatePhotosBlocks',
    ]);
}
