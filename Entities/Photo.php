<?php

namespace Modules\Instagramgrabber\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    // use Translatable;

    protected $table = 'instagramgrabber__photos';
    public $translatedAttributes = [];
    protected $fillable = [
    	'block_id',
			'shortcode',
			'hashtag',
			'thumbnail_src',
			'thumbnail150',
			'thumbnail240',
			'thumbnail320',
			'thumbnail480'
    ];

    // Get last photo in the block (random hashtag)
    public static function getRandomByBlock($blockId) {
        $photos = self::getByBlock($blockId);
        if ($photos->count())
    	   return $photos->random(1)->first();
        return null;
    }

    // Get all photos in the block (one per hashtag)
    public static function getByBlock($blockId) {
		return self::where('block_id', $blockId)->orderBy('id', 'desc')->groupBy('hashtag')->get();
    }

    // Get all photos in the block (ignore hashtag)
    public static function getAllByBlock($blockId) {
    	return self::where('block_id', $blockId)->orderBy('id', 'desc')->get();
    }

    // Get last photo in the block
    public static function getOneByBlock($blockId) {
    	return self::where('block_id', $blockId)->orderBy('id', 'desc')->first();
    }
}
