<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/instagramgrabber'], function (Router $router) {
    $router->bind('instagramgrabber', function ($id) {
        return app('Modules\Instagramgrabber\Repositories\InstagramGrabberRepository')->find($id);
    });
    $router->get('instagramgrabbers', [
        'as' => 'admin.instagramgrabber.instagramgrabber.index',
        'uses' => 'InstagramGrabberController@index',
        'middleware' => 'can:instagramgrabber.instagramgrabbers.index'
    ]);
    $router->post('instagramgrabber-update', [
        'as' => 'admin.instagramgrabber.settings.update',
        'uses' => 'InstagramGrabberController@update',
        'middleware' => 'can:instagramgrabber.instagramgrabbers.index'
    ]);
});
