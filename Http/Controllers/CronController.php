<?php

namespace Modules\Instagramgrabber\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Artisan;
use Modules\Instagramgrabber\Entities\Settings;
use Modules\Instagramgrabber\Entities\Photo;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class CronController extends AdminBaseController {
	public function __construct() {
	}

	public function updatePhotosBlocks() {
		$this->setPhotosAsOld();
		$this->scrapeNewPhotos();
		$this->deleteOldPhotos();
	}

	private function setPhotosAsOld() {
		Photo::where('is_old', false)->update(['is_old' => true]);
	}

	private function deleteOldPhotos() {
		if (Photo::where('is_old', false)->count()) {
			Photo::where('is_old', true)->delete();
			self::clearHTMLCache();
		}
	}

	private function scrapeNewPhotos() {
		$settings = Settings::all();

		// blocks foreach
		foreach ($settings as $setting) {
			$hashtags = explode(",", $setting->hashtag);

			foreach ($hashtags as $hashtag) {
				$hashtag = trim($hashtag);

				// scrape photos by hashtag
				$scraped_photos = $this->getPhotosByHashtag($hashtag);

				// save images to db
				foreach ($scraped_photos as $scraped_photo) {
					$scraped_photo["hashtag"] = $hashtag;
					$scraped_photo["block_id"] = $setting->id;
					$newPhoto = Photo::create($scraped_photo);
				}
			}
		}
	}

	private function scrapeInstaHash($hashtag) {

		$hashtag_url = 'https://www.instagram.com/explore/tags/' . $hashtag . '/';

		if ($this->getHttpResponseCode($hashtag_url) == "200") {
			$insta_source = file_get_contents($hashtag_url); // instagram tag url
			$shards = explode('window._sharedData = ', $insta_source);
			$insta_json = explode(';</script>', $shards[1]);
			$insta_array = json_decode($insta_json[0], true);

			return $insta_array; // this return a lot things print it and see what else you need
		}
	}

	private function getHttpResponseCode($url) {
		$headers = get_headers($url);

		return substr($headers[0], 9, 3);
	}

	private function getPhotosByHashtag($hashtag) {
		$results_array = $this->scrapeInstaHash($hashtag);
		$limit = 15; // provide the limit thats important because one page only give some images then load more have to be clicked
		$image_array = [];

		for ($i = 0; $i < $limit; $i++) {
			$image_data = [];
			$hashtag_data = $results_array['entry_data']['TagPage'][0]['graphql']['hashtag']['edge_hashtag_to_media'];

			if ($hashtag_data['count'] > $i && isset($hashtag_data['edges'][$i])) {
				$latest_array = $hashtag_data['edges'][$i]['node'];
				$image_data['shortcode'] = $latest_array['shortcode'];
				$image_data['thumbnail_src'] = $latest_array['thumbnail_src'];

				$thumbnails = $latest_array["thumbnail_resources"];
				$image_data['thumbnail150'] = isset($thumbnails[0]) ? $thumbnails[0]["src"] : null;
				$image_data['thumbnail240'] = isset($thumbnails[1]) ? $thumbnails[1]["src"] : null;
				$image_data['thumbnail320'] = isset($thumbnails[2]) ? $thumbnails[2]["src"] : null;
				$image_data['thumbnail480'] = isset($thumbnails[3]) ? $thumbnails[3]["src"] : null;

				array_push($image_array, $image_data);
			}
		}

		return $image_array;
	}

		// Clear HTML cache
	public static function clearHTMLCache() {
		if (array_has(Artisan::all(), "page-cache:clear")) {
			Artisan::call("page-cache:clear");
		}
	}
}
