<?php

namespace Modules\Instagramgrabber\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface InstagramGrabberRepository extends BaseRepository
{
}
