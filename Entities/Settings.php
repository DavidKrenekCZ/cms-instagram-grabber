<?php

namespace Modules\Instagramgrabber\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    // use Translatable;

    protected $table = 'instagramgrabber__settings';
    public $translatedAttributes = [];
    protected $fillable = [];
}
