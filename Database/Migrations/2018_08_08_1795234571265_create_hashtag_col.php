<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateHashtagCol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('instagramgrabber__photos', function (Blueprint $table) {
            $table->string('hashtag')->after('block_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('instagramgrabber__photos', function (Blueprint $table) {
            $table->dropColumn('hashtag');
        });
    }
}
