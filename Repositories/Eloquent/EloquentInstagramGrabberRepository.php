<?php

namespace Modules\Instagramgrabber\Repositories\Eloquent;

use Modules\Instagramgrabber\Repositories\InstagramGrabberRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentInstagramGrabberRepository extends EloquentBaseRepository implements InstagramGrabberRepository
{
}
