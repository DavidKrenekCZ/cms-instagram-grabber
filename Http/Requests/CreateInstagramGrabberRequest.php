<?php

namespace Modules\Instagramgrabber\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateInstagramGrabberRequest extends BaseFormRequest
{
    public function rules()
    {
        return [];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
