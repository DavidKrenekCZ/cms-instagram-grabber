<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstagramgrabberPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagramgrabber__photos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('block_id');
            $table->mediumText('shortcode');
            $table->mediumText('thumbnail_src');
            $table->boolean('is_old');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instagramgrabber__photos');
    }
}
