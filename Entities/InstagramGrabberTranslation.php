<?php

namespace Modules\Instagramgrabber\Entities;

use Illuminate\Database\Eloquent\Model;

class InstagramGrabberTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'instagramgrabber__instagramgrabber_translations';
}
