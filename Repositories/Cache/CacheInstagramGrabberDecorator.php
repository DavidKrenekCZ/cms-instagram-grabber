<?php

namespace Modules\Instagramgrabber\Repositories\Cache;

use Modules\Instagramgrabber\Repositories\InstagramGrabberRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheInstagramGrabberDecorator extends BaseCacheDecorator implements InstagramGrabberRepository
{
    public function __construct(InstagramGrabberRepository $instagramgrabber)
    {
        parent::__construct();
        $this->entityName = 'instagramgrabber.instagramgrabbers';
        $this->repository = $instagramgrabber;
    }
}
