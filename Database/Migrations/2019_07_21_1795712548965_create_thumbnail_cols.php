<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateThumbnailCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('instagramgrabber__photos', function (Blueprint $table) {
            $table->mediumText('thumbnail480')->after('thumbnail_src')->nullable()->default(null);
            $table->mediumText('thumbnail320')->after('thumbnail_src')->nullable()->default(null);
            $table->mediumText('thumbnail240')->after('thumbnail_src')->nullable()->default(null);
            $table->mediumText('thumbnail150')->after('thumbnail_src')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('instagramgrabber__photos', function (Blueprint $table) {
            $table->dropColumn(['thumbnail150', 'thumbnail240', 'thumbnail320', 'thumbnail480']);
        });
    }
}
