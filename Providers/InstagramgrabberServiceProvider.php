<?php

namespace Modules\Instagramgrabber\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Instagramgrabber\Events\Handlers\RegisterInstagramgrabberSidebar;
use Modules\Instagramgrabber\Composers\InstagramGrabberViewComposer;

class InstagramgrabberServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterInstagramgrabberSidebar::class);

        view()->composer("*", InstagramGrabberViewComposer::class);
    }

    public function boot()
    {
        $this->publishConfig('instagramgrabber', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Instagramgrabber\Repositories\InstagramGrabberRepository',
            function () {
                $repository = new \Modules\Instagramgrabber\Repositories\Eloquent\EloquentInstagramGrabberRepository(new \Modules\Instagramgrabber\Entities\InstagramGrabber());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Instagramgrabber\Repositories\Cache\CacheInstagramGrabberDecorator($repository);
            }
        );
// add bindings

    }
}
