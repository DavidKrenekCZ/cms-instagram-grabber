@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('instagramgrabber::instagramgrabbers.title.main') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('instagramgrabber::instagramgrabbers.title.main') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-xs-12">
            <div class="box box-primary">
                <div class="box-body">
                    <form action="{{ route("admin.instagramgrabber.settings.update") }}" method="post">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th>{{ trans('instagramgrabber::instagramgrabbers.table.block_name') }}</th>
                                    <th>{{ trans('instagramgrabber::instagramgrabbers.title.hashtag') }}</th>
                                </tr>
                                @foreach($settings as $setting)
                                    <tr>
                                        <td>{{ $setting->block_name }}</td>
                                        <td>
                                            <input type="hidden" name="id[]" value="{{ $setting->id }}">
                                            <input type="text" name="hashtag[]" value="{{ $setting->hashtag }}" size="40">
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <!-- /.box-body -->
                        </div>
                        <div class="text-center">
                            {{ csrf_field() }}
                            <input type="submit" class="btn btn-success btn-flat" value="Uložit">
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('openinghours::days.title.create day') }}</dd>
    </dl>
@stop

@push('js-stack')
<script>
</script>
@endpush

@push("css-stack")
<style type="text/css">
</style>
@endpush